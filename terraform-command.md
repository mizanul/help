
#Ansible Ad-hoc command

####Uptime of all the machines
```
ansible all -s -n shell -a 'uptime'
```

####Date of all the machines
```
ansible all -s -n shell -a 'date'
```

####redhat releases of all the machines
```
ansible all -s -n shell -a 'cat /etc/redhat-release'
```

####Kind of mount of all the machines
```
ansible all -s -n shell -a 'mount'
```

####Date of all the machines
```
ansible all -s -n shell -a 'date'
```



##Sample playbook

####myplaybook.yml

```
---
-host: webserver
 task:
  -name: Installs nginx web server
  apt: pkg=nginxstate=installed update_cache=true
  notify:
   -start nginx

 handlers:
  -name: start nginx
  service:name=nhinx state=started 

```  

####to deploy the ansible playbook, run the following command:
```
ansible-playbook myplaybook.yml
```

####install ansible
```
apt-get install ansible
```
####create ssh public key in the controller machine
```
ssh-keygen
```

####copy ssh key to the node machine
```
ssh-copy-id -i root@node-ip-address
```
