
#Puppet
####Puppet Master Commands
```
puppet cert list
```

###steps
####1. create a new SSL key for the puppet agent 
```
puppet agent -t
```

####2. go to puppet master  
```
puppet cert list
```
you should see the puppetagent cert

####3. sign the puppetagent cert
```
puppet cert sign puppetagent
```

####4. go back to puppet agent  
```
puppet agent -t
```


###resources(multiple) --->class-->manifest(multiple)-->module


####sample resource:

#####syntax:
```
resource_type {'resource_name'
	attribute => value
}
```

#####example:
```
file {'/etc/inetd.conf'
	ensure => 'etc/init/inetd.conf',
}
```

####example class
```
class example_class{
	code
}
```

####go back to puppet master
1. write 
```
gedit /etc/puppet/manifest/site.pp
```
2. write 
```
file {'/etc/inetd.conf'
	ensure => 'etc/init/inetd.conf',
}
```

3. go to puppet agent 
```
puppet agent -t
ls /etc/inetd.conf
```


###puppet masnifest:

```
class{ 'apache': 
	apache::vhost{ 'example.com':
		port => '80',
		docroot => '/var/www/html'
		}	
}

##practice:

1. go back to puppet master 
```
gedit /etc/puppet/manifest/site.pp
```
2. write

```
package { 'httpd':
	ensure => installed,
}
service { 'httpd':
	ensure => running,
}

2. go back to puppet agent

```
puppet agent -t
```

##MODULE

1. go back to puppet master
###install mysql
```
puppet module install puppetlabs-mysql --version 3.10.0
```
###install php
```
puppet module install mayflower-php --version 4.0.0-beta1
```


```
gedit /etc/puppet/manifest/site.pp

include '::mysql::server'
include '::php'
```

3. go back to puppet agent

```
puppet agent -t
```





