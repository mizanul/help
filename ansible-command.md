
#Ansible Ad-hoc command

####Uptime of all the machines
```
ansible all -s -n shell -a 'uptime'
```

####Date of all the machines
```
ansible all -s -n shell -a 'date'
```

####redhat releases of all the machines
```
ansible all -s -n shell -a 'cat /etc/redhat-release'
```

####Kind of mount of all the machines
```
ansible all -s -n shell -a 'mount'
```

####Date of all the machines
```
ansible all -s -n shell -a 'date'
```

###To ping host machines

```
ansible -m ping 'host-servers'
```

##Sample playbook

####myplaybook.yml

```
---
-host: webserver
 task:
  -name: Installs nginx web server
  apt: pkg=nginx state=installed update_cache=true
  -name: Installs apche2
  apt: name=apche2 update_cache=true

  notify:
   -start apche2
   -start nginx

 handlers:
  -name: start nginx
  service:name=nhinx state=started 
  -name: restart apche2
  service:name=apche2 state=restarted 

```  
###Sample LAMP playbook

####lampstack.yml

```
---
-name Install LAMP
 host: webserver
 become: true
 become_user: root
 gather_facts: true
 task:
   -name: "Install apache2"
	package: name=apache2 state=present
   -name: "Install apache2-php5"	
    package: name=libapache2-mod-php state=present
   -name: "Install php-cli"	
    package: name=php-cli state=present
   -name: "Install php-mycrypt"	
    package: name=php-mycrypt state=present
   -name: "Install php-gd"	
    package: name=php-gd state=present 
   -name: "Install php-mysql"	
    package: name=php-mysql state=present 
   -name: "Install mysqlserver"	
    package: name=mysql-server state=present    

``` 

###Sample mysql mudule playbook

####mysqlmodule.yml

```
---
-name Install MySQL Module
 host: all
 remote_user: root

 task:
   -name: Install "pip"
	apt: name=python-pip state=present
   -name: Install "libmysqlclient-dev"	
    apt: name=libmysqlclient-dev state=present
   -name: Install the Python MySQL module	
    pip: name=MySQL-python
   -name: Create database user mizanul
    mysql_user: name=mizanul password=password priv=*.*:ALL state=present
   -name: Create database isdi edu	
    mysql_db: db=edu state=present 
   -name: Create a Table reg	
    package: mysql -u mizanul -ppassword -e 'CREATE TABLE reg (name varchar(30),email varchar(30));' edu    

``` 

###Sample deploy playbook

####deploywebsite.yml

```
---
-name copy
 host: webserver
 become: true
 become_user: root
 gather_facts: true
 task:
   -name: "copy file"
	copy: src=/home/mizanul/Documents/index.html dest=/var/www/html/index.html
   -name: "copy file"
	copy: src=/home/mizanul/Documents/process.php dest=/var/www/html/process.php
   -name: "copy file"
	copy: src=/home/mizanul/Documents/result.php dest=/var/www/html/result.php    

```  

####to deploy the ansible playbook, run the following command:
```
ansible-playbook myplaybook.yml
```

####install ansible
```
apt-get install ansible
```
####create ssh public key in the controller machine
```
ssh-keygen
```

####copy ssh key to the node machine
```
ssh-copy-id -i root@node-ip-address
```
