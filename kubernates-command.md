
#Kubernates

###step-1
####On Master (Callico cni). To initialize Kubernetes master
```
sudo kubeadm init --pod-network-cidr=<cidr-of-pod> --apiserver-adverties-address=<ip-address-of-the-master>
```

###step-2
####To start using your cluster, you need to run the following commands as normal user
```
mkdir -p $HOME/.kube
sudo cp -t /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

```

###step-3
####create kubernetes POD based on Calico

```
kubectl apply -f https://docs.projectcalico.org/v3.0/getting-started/kubernetes/installation/hosted/kubeadm/1.7/calico.yaml
```

####to check the Nodes
```
kubectl get nodes
```

####to check the PODs
```
kubectl get pods --all-namespaces -o wide
```

###step-4
####create kubernetes dashboard

```
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```

###step-5
####create kubernetes dashboard

```
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```

###step-6
####To enable proxy and continues with new terminal window  

```
kubectl proxy
```
###step-7
####To create a service account for your dashboard  

```
kubctl create serviceaccount dashboard -n default
```

###step-8
####To add cluster binding rules for your roles on dashboard  

```
kubctl create clusterbinding dashboard-admin -n default \ 
--clusterrole=cluster-admin \
--serviceaccount-default:dashboard
```

###step-9
####To get the secret key to be pasted onto the dashboard token pwd. Copy the outcoming secret key  

```
kubctl get secret $(kubectl get serviceaccount dashboard -o jsonpath="{.secrets[0].name}") -o jsonpath="{.data.token}" | base64 --decode
```

#### Now goto http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubenetes-dashboard:/proxy/#!/login for accessing the dashboard



###Useful commands:
####for generating the token again, run in Master
```
sudo kubectl create --print-join-command
```

####On the Master, for deploying an NGINX App through CLI. Usually its done via YAML file.
```
kubectl create deployment nginx --image=nginx. //1st, create a deployment
kubectl get deployments //Verify the deployments
kubectl describe deployment nginx //More details about the deployments
kubectl create service nodeport nginx --tcp=80:80. //Create the service on the nodes
kubectl get svc // To ceck which deployment is running on which node
```


##Sample yaml file
###file name: deployment.yaml

####step-1 (create YAML file)
```
---
 appVersion: extensions/v1beta1
 kind: Deployment
 metadata:
 	name: mizan-web
 spec:
 	replicas: 2
 	template:
 	 metadata:
 	 	labels:
 	   	app: web
 	 spec:
 	  	containers:
 	  		-name: front-end
 	  		 image: nginx
 	  		 ports:
 	  		 	-containerPort: 80
 	  		-name: rss-reader
 	  		 image: nickchase/rss-php-nginx:v1
 	  		 ports:
 	  		 	-containerPort: 88 	

```

####step-2 (deploy)

```
kubectl create -f deployments.yaml
```
####step-3 (to delete deployment)

```
kubectl delete deployment mizan-web
```

